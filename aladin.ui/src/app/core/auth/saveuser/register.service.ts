import { Injectable } from '@angular/core';
import { RegisterData } from '../model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment} from '../../../../environments/environment.prod'
@Injectable({
  providedIn: 'root'
})
export class RegisterService {
url="users/auth/register";

constructor(private http:HttpClient) { }


  saveUser(data:RegisterData):Observable <any>{
    
    return this.http.post(environment.apiBaseUrl+this.url,data);
  }


  saveuserfirm(data:any){
    return this.http.post(environment.apiBaseUrl+"partnerfirms",data);
  }
  
  saveuseraccount(data:any){
    return this.http.post(environment.apiBaseUrl+'partneraccounts',data);
  }
 
}
