import { NgModule, SkipSelf ,Optional} from '@angular/core';
import { AddshapeService,AddclipsService,AddtextService ,TextcolorService} from './editor';
import { RegisterService,LoginService,FormvalidationService } from './auth';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule
  ],
  exports:[HttpClientModule,
    
  ],

  providers:[AddclipsService,AddshapeService,AddtextService,TextcolorService,RegisterService,LoginService,FormvalidationService],
})
export class CoreModule { 
 
  constructor(@Optional()@SkipSelf() core:CoreModule ){
    if (core) {
        throw new Error("You should import core module only in the root module")
    }
  }
}
 