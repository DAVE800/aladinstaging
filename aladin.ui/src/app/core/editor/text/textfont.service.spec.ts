import { TestBed } from '@angular/core/testing';

import { TextfontService } from './textfont.service';

describe('TextfontService', () => {
  let service: TextfontService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TextfontService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
