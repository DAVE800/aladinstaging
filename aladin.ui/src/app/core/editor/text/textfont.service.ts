import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TextfontService {

  constructor() { }


  saveImage(key:string,url:string){
    localStorage.setItem(key,url);

  }


  getImage(key:string){
    return localStorage.getItem(key);

  }
}
