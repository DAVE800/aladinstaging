import { TestBed } from '@angular/core/testing';

import { TextcolorService } from './textcolor.service';

describe('TextcolorService', () => {
  let service: TextcolorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TextcolorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
