import { TestBed } from '@angular/core/testing';

import { AddtextService } from './addtext.service';

describe('AddtextService', () => {
  let service: AddtextService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddtextService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
