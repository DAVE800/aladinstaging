import { Injectable } from '@angular/core';
import { fabric } from 'fabric';
import { install } from 'chart-js-fabric';
install(fabric);
import { Observable } from 'rxjs';
import { Callbacks } from 'fabric/fabric-impl';
declare var jQuery: any;
declare var exports:any
@Injectable({
  providedIn: 'root'
})
export class AddtextService {
Text:any;
  constructor() { }

 addText(obj:any): Observable<any>{
  this.Text= new fabric.IText('Ecrivez votre text ici',{
    top:150,
    left:200,
    fill:'blue',
    fontStyle:'normal',
    fontSize:48,
    fontFamily:"Time new roman"
  });
  obj.add(this.Text).setActiveObject(this.Text);
  return this.Text;

 }

 curvetext(){
	 (function ($) {
      $(document).ready(function(){
        console.log("Hello from jQuery!");
		
      });
    })(jQuery);
 }




}
