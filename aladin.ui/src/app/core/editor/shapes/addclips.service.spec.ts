import { TestBed } from '@angular/core/testing';

import { AddclipsService } from './addclips.service';

describe('AddclipsService', () => {
  let service: AddclipsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddclipsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
