import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
//const fs = require('move-file')

@Injectable({
  providedIn: 'root'
})
export class AddshapeService {

  constructor(private http:HttpClient) { }

add(data:any)
  {
    const formdata = new FormData();
   formdata.append('link',data,data.name);
   console.log(formdata)
    return this.http.post("http://localhost:8000/uploads",formdata,{
      reportProgress: true,
      observe: 'events'
    });
  }


  getshape(){
    return this.http.get("http://localhost:8000/uploads/list")
  }
}
