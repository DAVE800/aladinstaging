import { TestBed } from '@angular/core/testing';

import { AddshapeService } from './addshape.service';

describe('AddshapeService', () => {
  let service: AddshapeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddshapeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
