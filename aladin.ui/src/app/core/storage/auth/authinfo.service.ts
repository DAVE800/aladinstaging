import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthinfoService {

  constructor() { }

  setItem(name:string,data:any){
    localStorage.setItem(name,data);
  }

removeItem(name:string):boolean{
  if(this.getItem(name)!=null && this.getItem!=undefined){
    localStorage.removeItem(name);
    return true

  }else{
    return false
  }

}

getItem(key:string) {
  return localStorage.getItem(key);
}


}
