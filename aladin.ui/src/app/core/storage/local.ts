import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  constructor() { 

  }
  items = (() => {
    const fieldValue = localStorage.getItem('designs');
    return fieldValue === null
      ? []
      : JSON.parse(fieldValue);
  })();
  
  cart_items = (() => {
    const fieldValue = localStorage.getItem('cart');
    return fieldValue === null
      ? []
      : JSON.parse(fieldValue);
  })();

  save () {
    localStorage.setItem("designs", JSON.stringify(this.items));
  }

  add(dataset:any) {
    this.items.push(dataset);
    this.save();
  }

  adtocart(data:any){
    this.cart_items.push(data);
    this.savec();
  }

  savec(){
    localStorage.setItem("cart",JSON.stringify(this.cart_items));

  }
removeItem(id:any){
  this.cart_items.splice(+id,1);
  this.savec();

}

removeOne(id:any){
  this.items.splice(+id,1);
  this.save();
}


}
