import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormvalidationService ,RegisterService} from 'src/app/core';
@Component({
  selector: 'app-register-partner',
  templateUrl: './register-partner.component.html',
  styleUrls: ['./register-partner.component.scss']
})
export class RegisterPartnerComponent implements OnInit {
 isfirst=true;
 isthird=false;
 issecond=false;
 partner_id=0;
account={
  pmode:"",
  accountnmb:0,
  bcode:"",
  ibn:"",
  mobile:""
}
 userinfo={
   lname:"",
   fname:"",
   email:"",
   phone:"",
   wht:"",
   city:"",
   pwd:"",
   cpwd:"",
   is_partner:true,

 }
 show=false;
 userfirm={
   sr:"",
   l:"",
   d:"",
   f:"SARL",
   rgnb:"",
   ctry:"",
   
 }
File:any;
 message={
   phone:"",
   pwd:"",
   email:""
 }
cpt=0;
cp=0;
c=0;
 err={
   pwd:false,
   phone:false,
   email:false
   
 }
  constructor(private validate:FormvalidationService,private registerservice:RegisterService) { }

  ngOnInit(): void {
  }



  follow_second(event:Event){
    if(this.cpt==0){
      this.check_first();

    }else{
      this.isfirst=!this.isfirst;
      this.issecond=!this.issecond;
    }
    
  }

  back_second(){
    if(this.isthird){
      this.isthird=!this.isthird;
      this.issecond=!this.issecond;
      this.cp=+1;

    }
  }

back_first(){
  if(this.issecond){
    this.isfirst=!this.isfirst;
    this.issecond=!this.issecond;
    this.cpt=+1;
  }
}

  follow_third(){
    if(this.cp==0){
      this.check_second();

    }else{
      this.isthird=!this.isthird;
      this.issecond=!this.issecond;
    }
  }


check_first(){
  if(!this.validate.validatePassword(this.userinfo.pwd,this.userinfo.cpwd)){
    this.err.pwd=!this.err.pwd
    this.message.pwd="passwords do not match";
   
  }

  this.toggleSpinner();
  this.registerservice.saveUser({name:this.userinfo.fname+" " + this.userinfo.lname,email:this.userinfo.email,phone:this.userinfo.phone,is_partner:this.userinfo.is_partner,whatsapp:this.userinfo.wht,password:this.userinfo.pwd,city:this.userinfo.city}).subscribe(
    res=>{
      this.toggleSpinner();
        this.isfirst=!this.isfirst;
        this.issecond=!this.issecond; 
        this.partner_id=res.user.user_id;  
    },
    err=>{
      this.toggleSpinner();
      console.log(err)
   if(err.error.phone){
       this.err.phone=!this.err.phone;
       this.message.phone=err.error.phone;
     
     }

     if(err.error.password){
       this.err.pwd=!this.err.pwd;
       this.message.pwd=err.error.password;     
     }
     if(err.error.email){
       this.err.email=!this.err.email;
       this.message.email=err.error.email;

           } 
   }); 
}
  check_second(){
    if(this.userfirm.sr!=""){
      let data ={
        sr:this.userfirm.sr,
        city:this.userfirm.l,
        ctry:this.userfirm.ctry,
        firm_num:this.userfirm.rgnb,
        user_id:+this.partner_id
      }
      this.toggleSpinner();
      this.registerservice.saveuserfirm(data).subscribe(
res=>{
  this.toggleSpinner();
  this.issecond=!this.issecond; 
  this.isthird=!this.issecond;
},
err=>{
  this.toggleSpinner();
  console.log(err)
  });
    }
}

toggleSpinner(){
  this.show = !this.show;

}

saveuseraccount(){
  let data={
    pmode:this.account.pmode,
    accountnmb:this.account.accountnmb,
    bcode:this.account.bcode,
    ibn:this.account.ibn,
    mobile:this.account.mobile,
    user_id:+this.partner_id
  }
  this.toggleSpinner();
  this.registerservice.saveuseraccount(data).subscribe(res=>{
   localStorage.setItem("access_token",'1');
   localStorage.setItem('part',JSON.stringify(this.partner_id));
   location.href="/partners/dashboard";

  },
  err=>{
    this.toggleSpinner();
    console.log(err)
  }
  );
 
}

}
