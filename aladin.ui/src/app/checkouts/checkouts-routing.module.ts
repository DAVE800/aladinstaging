import { CheckoutVideComponent } from './checkout-vide/checkout-vide.component';
import { NgModule } from '@angular/core';
import { CheckoutComponent } from './checkout/checkout.component';
import { RouterModule, Routes } from '@angular/router';
import { PaiementComponent } from './paiement/paiement.component';

const routes: Routes = [
  {path:':id', component:CheckoutComponent},
  {path:'checkout-vide', component:CheckoutVideComponent},
  {path:'pay',component:PaiementComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutsRoutingModule { }
