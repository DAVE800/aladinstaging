import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AladineditorComponent } from './aladineditor/aladineditor.component';
import { SharedModule } from '../shared';
import { ContextMenuModule } from 'ngx-contextmenu';


import { EditorRoutingModule } from './editor-routing.module';
import { DesignsComponent } from './designs/designs.component';
import { ProductsComponent } from './products/products.component';
import { DetailsComponent } from './details/details.component';
import { FacesComponent } from './faces/faces.component';
import { FormeComponent } from './forme/forme.component';
import { ClipartComponent } from './clipart/clipart.component';
@NgModule({
  declarations: [
    AladineditorComponent,
    DesignsComponent,
    ProductsComponent,
    DetailsComponent,
    FacesComponent,
    FormeComponent,
    ClipartComponent,
  
  ],
  imports: [
    EditorRoutingModule,
    SharedModule,
    ContextMenuModule.forRoot()
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA ]
})
export class EditorModule { }
