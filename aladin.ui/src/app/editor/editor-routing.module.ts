import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AladineditorComponent } from './aladineditor/aladineditor.component';

const routes: Routes = [
  {
  path:':name/:id',
  component: AladineditorComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditorRoutingModule { }
