import { Component, Input, OnInit ,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-designs',
  templateUrl: './designs.component.html',
  styleUrls: ['./designs.component.scss']
})
export class DesignsComponent implements OnInit {
@Input() url:any;
@Output() newItemEvent =new EventEmitter<string>();

constructor() { }

  ngOnInit(): void {

  }

  gotocart(event:any){
    this.newItemEvent.emit(event);

  
  }

}
