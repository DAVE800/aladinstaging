import { Component, OnInit ,OnChanges,ViewChild} from '@angular/core';
import { fabric } from 'fabric';
import { install } from 'chart-js-fabric';
install(fabric);
import {  ActivatedRoute } from '@angular/router';
import { AddtextService,TextfontService,AddshapeService,ListService,LocalService } from '../../core';
declare var require: any
var FontFaceObserver = require('fontfaceobserver');
var myalert=require('sweetalert2')
var $ = require("jquery");
import { ContextMenuComponent } from 'ngx-contextmenu';

  @Component({
    selector: 'app-aladineditor',
    templateUrl: './aladineditor.component.html',
    styleUrls: ['./aladineditor.component.scss']
  })
  export class AladineditorComponent implements OnInit,OnChanges {
    isp:boolean =true;
    canvas:any;
    rect:any;
    s:any;
    face1:any;
    dt:any;
    red:any;
    is_underlined=false;
    backtext:any=[];
    shapes:any;
    selectedDay="blue";
    isCliked=false;
    isbold:boolean =false;
    Text:any;
    url:any;
    stroke_size=1;
    strokecolor="#0B44A5"
    Textcolor="#0B44A5";
    stroke_color="blue"
    public items = [
      { name: 'delete', otherProperty: 'Foo' },
      { name: 'copy', otherProperty: 'Bar' }
  ];
    name:any;
    inpunmb=1;
    divider=true
    text_height:any;
    text_width:any;
    imgsrc:any;
    selecetdFile:any;
    public fontFace: any;
    imagePreview:any;
    fonts = ["Choix de police","Arial","roboto",'Tangerine','caveat',"poppins","Dancing Script","cinzel","Festive","satisfy","Yellowtail","pacifico","monoton","Oleo Script","Pinyon Script"];
    designed:any=[];
    price:any;
    para:any;
    logo:any;
    prodid:any;
    iscat:any;
    Image:any;
    show=false;

    @ViewChild(ContextMenuComponent) public basicMenu:any= ContextMenuComponent;

    constructor(private Url:TextfontService,private add:AddtextService,private route: ActivatedRoute,private shp:AddshapeService,private p:ListService,private L:LocalService ) { }
    ngOnChanges():void{
      if(localStorage.getItem('price')){
        this.price=localStorage.getItem('price');
      }

    }
    showMessage(message: any) {
      console.log(message);
    }

    toggle(){
      this.show=!this.show;
    }
    OnRightClick(event:any){
     
      
      let item =this.canvas.getActiveObject();
      if(item){
        var elt=document.getElementById('xdv')
        this.p.triggerMouse(elt)   
      }
      
      return false
    }
    ngOnInit(): void {
     
    this.canvas = new fabric.Canvas('aladin-editor',{ 
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,    
    });
    this.canvas.filterBackend=new fabric.WebglFilterBackend();
    this.name=this.route.snapshot.paramMap.get('id');
    this.para =this.route.snapshot.paramMap.get('name');

  let text_h= document.getElementById("Range3")
  let text_w =document.getElementById("customRange3")
  text_h?.addEventListener('input',()=>{
    let text = this.canvas.getActiveObject();
          text.set({fontSize:this.text_height,height:this.text_height});
  });
  document.getElementById('colorc')?.addEventListener('input',()=>{
    let item = this.canvas.getActiveObject();
    item.set({fill:this.Textcolor});
  });
  document.getElementById('strokesize')?.addEventListener('input',()=>{
    this.canvas.getActiveObject().set({strokeWidth:this.stroke_size,stroke:this.stroke_color});
  });
  document.getElementById('strokecolor')?.addEventListener('input',()=>{
    this.canvas.getActiveObject().set({strokeWidth:this.stroke_size,stroke:this.stroke_color});
  });

  text_w?.addEventListener("input",()=>{
    let text=this.canvas.getActiveObject();
          text.set({width:this.text_width});
          text.set({fontSize:this.text_width});
        })

   if(this.para=="cloth"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getcloth(this.name).subscribe(res=>{
      this.dt=res;    
      for(let elt of this.dt){
       this.price=elt.price;
       this.s=elt.img;
       this.face1=elt.customimg
       localStorage.setItem('price',elt.price);
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.makeImage(this.imgsrc);
        this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
       }else{
         this.backtext.push(elt.text)
       }
       
      }
    },
    err=>{console.log(err)}
    );
   }

   if(this.para=="gadgets"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getGadget(this.name).subscribe(
      res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.price=elt.price;
       this.s=elt.img;
       this.face1=elt.customimg
        if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        localStorage.setItem('price',elt.price);
        this.makeImage(this.imgsrc);
        this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
       }
      
      }

    },
    err=>{console.log(err)}
    );
   }

   let ofont:any=[]
   this.p.getfonts().subscribe(
     res=>{
       ofont=res
       if(ofont.status==200){
         for(let item of ofont.fonts){
           this.fonts.push(item.name);
           this.p.addStylesheetURL(item.url);
         }
         this.fonts.unshift('Times New Roman');
         var select = document.getElementById("family");
         this.fonts.forEach(function(font) {
         var opt = document.createElement('option');
          opt.innerHTML=font;
          opt.value=font;
          select?.appendChild(opt);
            });
            var select = document.getElementById("font-family");
            this.fonts.forEach(function(font) {
            var opt = document.createElement('option');
             opt.innerHTML=font;
             opt.value=font;
             select?.appendChild(opt);
  
  
               }
               );
       }
     },
     err=>{
       console.log(err)
     }
   )
   if(this.para=="disps"){
    this.iscat=this.para;
    this.prodid=this.name;

    this.p.getDisp(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.price=elt.price;
       this.s=elt.img;
       this.face1=elt.customimg;
       localStorage.setItem('price',elt.price)
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.makeImage(this.imgsrc);
        this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
      
      }
       


      }
    },
    err=>{console.log(err)}
    );
   }


   if(this.para=="prints"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getprint(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.price=elt.price;
       this.s=elt.img;
       this.face1=elt.customimg

       localStorage.setItem('price',elt.price)
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.makeImage(this.imgsrc);
        this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
       }
       
      }
    },
    err=>{console.log(err)}
    );
   }

   if(this.para=="packs"){
    this.iscat=this.para;
    this.prodid=this.name;
    this.p.getPack(this.name).subscribe(res=>{
      this.dt=res
      for(let elt of this.dt){
       this.s=elt.img;
       this.price=elt.price;
       this.face1=elt.customimg

       localStorage.setItem('price',elt.price)
       if(+elt.is_back==0){
        this.imgsrc=elt.front_side;
        this.makeImage(this.imgsrc);
        this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
       }
       
      }
    },
    err=>{console.log(err)}
    );
   }
   if(this.para=="tops"){
    this.iscat=this.para;
    this.prodid=this.name;

    this.p.gettop(this.name).subscribe(res=>{
      this.dt=res;
      for(let elt of this.dt){
       this.imgsrc=elt.img;
       this.s=elt.img;
       this.price=elt.price;
       localStorage.setItem('price',elt.price)
       this.makeImage(this.imgsrc);


      }
    },
    err=>{console.log(err)}
    );



   }

   
    
    }

underline(){
  let item=this.canvas.getActiveObject()
  if(!item.underline){
    item.set({underline:true})
  }else{
    item.set({underline:false})

  }

}
turnImage(event:any){
  let im=event.target.id
  console.log(im)
  var objt=this.canvas.getObjects()
  for(let i=0;i<objt.length;i++){
    this.canvas.remove(objt[i])
  }
  this.canvas.renderAll()
console.log(this.prodid,this.iscat)
//vetements
if(this.iscat=="cloth"){
  this.iscat=this.iscat;
this.prodid=this.prodid;
this.p.getcloth(this.prodid).subscribe(res=>{
this.dt=res;    
for(let elt of this.dt){
this.price=elt.price;
this.s=elt.img;
this.face1=elt.customimg

localStorage.setItem('price',elt.price);
if(+elt.is_back ==1 && im=="back"){
   
this.imgsrc=elt.customimg;
this.makeImage(this.imgsrc);
this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
}
if(+elt.is_back== 0 && im == "front"){
  this.imgsrc=elt.front_side;
  this.makeImage(this.imgsrc);
  this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
}

}

},
err=>{
  console.log(err)
}
);
}


//fin vetements printed


if(this.iscat=="prints"){
  this.iscat=this.iscat;
  this.prodid=this.prodid;
  this.p.getprint(this.prodid).subscribe(res=>{
    this.dt=res;
    for(let elt of this.dt){
     this.price=elt.price;
     this.s=elt.img;
     this.face1=elt.customimg;

     localStorage.setItem('price',elt.price);

     if(+elt.is_back==1&&im=="back"){

      this.imgsrc=elt.customimg;

      this.makeImage(this.imgsrc);

      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);

     }
     if(+elt.is_back==0&&im=="front"){
      this.imgsrc=elt.front_side;
      this.makeImage(this.imgsrc);
      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
    
    }
     
    }
  },
  err=>{console.log(err);}
  );
 }
//fin printed gadgets


if(this.iscat=="gadgets"){
  this.iscat=this.iscat;
  this.prodid=this.prodid;

  this.p.getGadget(this.prodid).subscribe(res=>{
    this.dt=res;
    for(let elt of this.dt){
     this.price=elt.price;
     this.s=elt.img;
     this.face1=elt.customimg;

      if(+elt.is_back==1&&im=="back"){
      this.imgsrc=elt.customimg;
      this.makeImage(this.imgsrc);
      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
     }
     if(+elt.is_back==0&&im=="front"){
      this.imgsrc=elt.front_side;
      this.makeImage(this.imgsrc);
      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke)
    }
    
     localStorage.setItem('price',elt.price);


    }
  },
  err=>{console.log(err);
  }
  );
 }

 //disps moonth
 
 if(this.iscat=="disps"){
  this.iscat=this.iscat;
  this.prodid=this.prodid;

  this.p.getDisp(this.prodid).subscribe(res=>{
    this.dt=res;
    for(let elt of this.dt){
     this.price=elt.price;
     this.s=elt.img;
     this.face1=elt.customimg;

     localStorage.setItem('price',elt.price)
     if(+elt.is_back==1&&im=="back"){
      this.imgsrc=elt.customimg;
      this.makeImage(this.imgsrc);
      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
     }
     if(+elt.is_back==0&&im=="front"){
      this.imgsrc=elt.front_side;
      this.makeImage(this.imgsrc);
      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
    }
     


    }
  },
  err=>{console.log(err)}
  );
 }
 //packs
 if(this.iscat=="packs"){
  this.iscat=this.iscat;
  this.prodid=this.prodid;
  this.p.getPack(this.prodid).subscribe(res=>{
    this.dt=res
    for(let elt of this.dt){
     this.s=elt.img;
     this.price=elt.price;
     this.face1=elt.customimg;
     localStorage.setItem('price',elt.price)
     if(+elt.is_back==1&&im=="back"){
      this.imgsrc=elt.customimg;
      this.makeImage(this.imgsrc);
      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
     }
     if(+elt.is_back==0&&im=="front"){
      this.imgsrc=elt.front_side;
      this.makeImage(this.imgsrc);
      this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
    }
     
    }
  },
  err=>{console.log(err)}
  );
 }
}
    selectChangeHandler(event: any) {
      let item= this.canvas.getActiveObject();
      this.Text=item
      this.loadfont(event.target.value);
      if(this.Text)
      this.Text.set({
        fontFamily:event.target.value
      })

    }
    
    setTextBeforeEdit(){
      let text = this.canvas.getActiveObject()
      if(text){
        if(text.underline&&text._textBeforeEdit){
          text.set({text:text._textBeforeEdit,underline:false});
        }
        
        if(text._textBeforeEdit){
          text.set({text:text._textBeforeEdit});
        }
      }
    }

    removeItem(){
      let item = this.canvas.getActiveObject();
      this.canvas.remove(item);
    }

    MakeItalic(event:any){
      let item= this.canvas.getActiveObject();
      this.Text=item;
      if(this.Text!=undefined){
        if(this.isCliked){
          this.Text.set({fontStyle:'normal'});
          this.canvas.renderAll(this.Text)
          this.canvas.requestRenderAll()
          this.isCliked=!this.isCliked;
        }else{
          this.Text.set({fontStyle:'italic'});
          this.canvas.renderAll(this.Text);
          this.canvas.requestRenderAll();
          this.isCliked=!this.isCliked;
        }
       

      }

    }

    MakeBold(event:any){
      let item= this.canvas.getActiveObject();
      this.Text=item
      if(this.Text!=undefined){
        if(this.isbold){
          this.Text.set({fontWeight:'bold'});
          this.canvas.renderAll(this.Text);
          this.canvas.requestRenderAll();
          this.isbold=!this.isbold;

        }else{
          this.Text.set({fontWeight:'normal'});
          this.canvas.renderAll(this.Text);
          this.canvas.requestRenderAll();
          this.isbold=!this.isbold;
        }
      
      
      }
    }


    changeTextColor(event:any){
      this.selectedDay = event.target.value;
      let item= this.canvas.getActiveObject();
      item.set({fill:this.selectedDay});
      this.canvas.renderAll(item);
      this.canvas.requestRenderAll();
  
    }

    setImage(event:any){
      let id=event.target.id;
      let m=event.target.name;
      console.log(this.canvas.getObjects())
      var objt=this.canvas.getObjects()
      for(let i=0;i<objt.length;i++){
        this.canvas.remove(objt[i]);
      }
      this.canvas.renderAll()
      if(id!=undefined&&m!=undefined){
        //vetements
        if(m=="cloth"){
          this.iscat=m;
           this.prodid=id;
           this.p.getcloth(id).subscribe(res=>{
          this.dt=res;    
          for(let elt of this.dt){
          this.price=elt.price;
          this.s=elt.img;
          this.face1=elt.customimg;

          localStorage.setItem('price',elt.price);
          if(+elt.is_back==0){
              
        
            this.imgsrc=elt.front_side;
            this.makeImage(this.imgsrc);
            this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
          }else{
            this.backtext.push(elt.text);
          }
       
      }
    },
    err=>{console.log(err);}
    );
    }
     //gadgets
      if(m=="gadgets"){
        this.iscat=m;
        this.prodid=id;
    
        this.p.getGadget(id).subscribe(res=>{
          this.dt=res;
    
          for(let elt of this.dt){
           this.price=elt.price;
           this.s=elt.img;
           this.face1=elt.customimg;
    
            if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.makeImage(this.imgsrc);
            this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
           }else{
            this.backtext.push(elt.text);
          }
          
           localStorage.setItem('price',elt.price);
    
    
          }
        },
        err=>{console.log(err)}
        );
       }
    
       //disps
       if(m=="disps"){
        this.iscat=m;
        this.prodid=id;
    
        this.p.getDisp(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.price=elt.price;
           this.s=elt.img;
           this.face1=elt.customimg;
    
           localStorage.setItem('price',elt.price)
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.makeImage(this.imgsrc);
            this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
           }else{
            this.backtext.push(elt.text);
          }
           
          }
        },
        err=>{console.log(err);}
        );
       }
       //packs
       if(m=="packs"){
        this.iscat=m;
        this.prodid=id;
        this.p.getPack(id).subscribe(res=>{
          this.dt=res
          for(let elt of this.dt){
           this.s=elt.img;
           this.price=elt.price;
           this.face1=elt.customimg;
    
           localStorage.setItem('price',elt.price)
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.makeImage(this.imgsrc);
            this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
           }else{
            this.backtext.push(elt.text);
          }
           
          }
        },
        err=>{console.log(err)}
        );
       }
       //printed
       if(m=="prints"){
        this.iscat=m;
        this.prodid=id;
        this.p.getprint(id).subscribe(res=>{
          this.dt=res;
          for(let elt of this.dt){
           this.price=elt.price;
           this.s=elt.img;
           this.face1=elt.customimg;
    
           localStorage.setItem('price',elt.price);
           if(+elt.is_back==0){
            this.imgsrc=elt.front_side;
            this.makeImage(this.imgsrc);
            this.makeText(elt.fill,elt.fontfamily,elt.text,+elt.y_value,+elt.x_value,elt.h_y,elt.w_x,elt.stroke,elt.w_stroke);
           }else{
            this.backtext.push(elt.text);
          }
           
          }
        },
        err=>{console.log(err);}
        );
       }
//this.makeImage(event.target.src);
        //this.prodid=id;
        //this.iscat=m;

      }

    }
 
  makeImage(src:any,mcolor:string =""):any{
      var img = new Image();
      let canvas= this.canvas;
      img.src=src
      img.crossOrigin = 'anonymous';
      this.Image=img;
      if(this.show!=true){

               img.onload = function() {
                var f_img = new fabric.Image(img);
            if(mcolor !="" && mcolor != undefined){
              f_img.filters?.push(new fabric.Image.filters.BlendColor({
                color:mcolor, 
                mode: 'tint',
                alpha:0.7
            }));
            f_img.applyFilters()
      
            }
          
                canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
                    backgroundImageOpacity: 0.1,
                    backgroundImageStretch: false,
                    left: 10,
                    top: 5,
                    height:600,
                    width:800,
                });
        }

      }else{

        img.onload = function() {
          var f_img = new fabric.Image(img);

      if(mcolor !="" && mcolor !=undefined ){
        f_img.filters?.push(
          new fabric.Image.filters.BlendColor({
          color:mcolor, 
          mode: 'tint',
          alpha:0.7
      }));
      f_img.applyFilters()

      }
    
          canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
              backgroundImageOpacity: 0.1,
              backgroundImageStretch: false,
              left: 10,
              top: 5,
              height:600,
              width:800,
          });
        }
     // this.url=canvas.toDataURL();

      }
  

     }

     prodcolor(event:any){
     var col=event.target.id;
     if(col && this.Image.src){
      this.makeImage(this.Image.src,col);

    }

     }
  
getDesigns(){  
    this.makeImage(this.url&&this.isp);
    this.ispersonnalized();
    this.designed.push(this.url);
    console.log(this.designed);

    if(this.Text){
      this.canvas.remove(this.Text);

    }
    if(this.logo){
      this.canvas.remove(this.logo);

    }
    
}

ispersonnalized():boolean{
  return this.isp=!this.isp;

}
createText(){
  this.loadfont("pacifico");
  this.makeText('blue',"pacifico",'Je personnalise',250,130,10,41," ",0);
}

  makeText(fill:any,font:any,text:string,top:number,left:number,height:number,width:number,strok:string,strokwidth:number){
      this.Text= new fabric.IText(text,{
        top:top,
        left:left,
        fill:fill,
        fontStyle:'normal',
        //fontSize:48,
        cornerStyle:'circle',
        fontFamily:font,
        stroke:strok,
        height:height,
        width:width,
        strokeWidth:strokwidth
        
      });
      
      this.canvas.add(this.Text).setActiveObject(this.Text);
      this.canvas.renderAll(this.Text);
      this.canvas.requestRenderAll();
      
  }

ChangeImage(event:any){
  this.makeImage(event.target.src);
}



setLogo(event:any){
var imgInstance = new fabric.Image(event.target, {
  left: 200,
  top: 100,
  width:350,
  height:300
});
this.canvas.add(imgInstance).setActiveObject( imgInstance);
this.logo=imgInstance;
}

onFileUpload(event:any){
this.selecetdFile = event.target.files[0];
 const reader = new FileReader();
 reader.onload = () => {
  this.imagePreview = reader.result;
  };
  reader.readAsDataURL(this.selecetdFile);
   
  }

  

  onUpload(){
    let file:File=this.selecetdFile
    this.shp.add(file).subscribe(
      res=>{console.log(res)},
      err=>{console.log(err)}
    );

  }

  
  getPrice(){
    let price=localStorage.getItem('price')
      this.inpunmb=+this.inpunmb+1;
      if(this.inpunmb>0&&price!=null){
        this.price= this.inpunmb*(+price)
      }
    }


  getPricem(){
  let price=localStorage.getItem('price')
  this.inpunmb=this.inpunmb-1;
  if(this.inpunmb>0 && price!=null){
    if(((+this.inpunmb)*(+this.price))>0){
      this.price= ((+this.inpunmb)*(+price))
    }

  }else{
    this.inpunmb=this.inpunmb+1;

  }
    }


  addtocart(){
    let err=false
    let cart_data={
      in_stock:"En stock",
      qty:this.inpunmb,
      price:localStorage.getItem('price'),
      t:this.price
    };
    if(this.Text || this.logo){
      this.url=this.canvas.toDataURL();
      Object.assign(cart_data,{img:this.url})
      this.L.adtocart(cart_data);
      if(!err){
        myalert.fire({
          title: '<strong>produit ajouté</strong>',
          icon: 'success',
          html:
            '<h6 style="color:blue">Felicitation</h6> ' +
            '<p style="color:green">Votre design a été ajouté dans le panier panier</p> ' +
            '<a href="/cart">Je consulte mon panier</a> ' 
            ,
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> like!',
          confirmButtonAriaLabel: 'Thumbs up, great!',
          cancelButtonText:
            '<i class="fa fa-thumbs-down"></i>',
          cancelButtonAriaLabel: 'Thumbs down'
        })
      }

    }else{
      err=!err
      if(err){
        myalert.fire({
          icon: 'error',
          title: 'erreur...',
          text: 'veillez personnaliser le produit!',
        })
      }
    }
  
   
  }

loadfont(f:any){
    let fonts=new FontFaceObserver(f);
    fonts.load().then(function () {

    }).catch((err:any)=>{
      console.log(err);
    });
  }



Copy() {
  let canvas=this.canvas;

	// clone what are you copying since you
	// may want copy and paste on different moment.
	// and you do not want the changes happened
	// later to reflect on the copy.
	canvas.getActiveObject().clone(function(cloned:any) {
		canvas._clipboard= cloned;
	});
}


Paste() {
  let canvas=this.canvas
	// clone again, so you can do multiple copies.
	canvas._clipboard.clone(function(clonedObj:any) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + 15,
			top: clonedObj.top + 15,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			// active selection needs a reference to the canvas.
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj:any) {
				canvas.add(obj);
			});
			// this should solve the unselectability
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		canvas._clipboard.top += 10;
		canvas._clipboard.left += 10;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
}



  }