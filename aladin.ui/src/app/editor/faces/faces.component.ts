import { Component, OnInit ,Input,OnChanges,EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-faces',
  templateUrl: './faces.component.html',
  styleUrls: ['./faces.component.scss']
})
export class FacesComponent implements OnInit,OnChanges {
@Input() URL:any;
@Input() face1:any
@Output() newItemEvent =new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }
ngOnChanges(){

}
addNewItem(value: any) {
  this.newItemEvent.emit(value);
}
}
