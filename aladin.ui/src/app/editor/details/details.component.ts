import { Component, OnInit,Input ,OnChanges} from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnChanges  {
 @Input() myid:any;
 @Input() cat:any;
 detail:any;
 isname_cloth=true;
 isname_print=false;
 isname_gad=false;
 isname_disp=true;
 isname_pack=false;
price=0;
constructor(private l:ListService) { }

  ngOnChanges(): void {
    if(+this.myid){  
      if(this.cat=="cloth"){
        this.isname_cloth=!this.isname_cloth;
        this.l.getcloth(this.myid).subscribe(
          res=>{
            this.detail=res;
            for(let elt of this.detail){
              this.price=elt.price;
              localStorage.setItem('price',elt.price)
             }
          },
          err=>{console.log(err)}
        )
      }
  
      if(this.cat=="disps"){
        this.isname_disp=!this.isname_disp;

        this.l.getDisp(this.myid).subscribe(
          res=>{
            this.detail=res;
            for(let elt of this.detail){
              this.price=elt.price;
              localStorage.setItem('price',elt.price)
             }
          
          },
          err=>{console.log(err)}
        )
      }
  
      if(this.cat=="prints"){
        this.isname_print=!this.isname_print;

        this.l.getprint(this.myid).subscribe(
          res=>{
            this.detail=res;
            for(let elt of this.detail){
              this.price=elt.price;
              localStorage.setItem('price',elt.price)
             }
          }
          
          ,
          err=>{console.log(err)}
        )
      }
  
      if(this.cat=="gadgets"){
        this.isname_gad=!this.isname_gad

        this.l.getGadget(this.myid).subscribe(
          res=>{
            this.detail=res;
            for(let elt of this.detail){
              this.price=elt.price;
              localStorage.setItem('price',elt.price)
             }
          },
          err=>{console.log(err)}
        )
      }
  
      if(this.cat=="tops"){
        this.l.gettop(this.myid).subscribe(
          res=>{
            this.detail=res;
          }
          ,
          err=>{console.log(err)}
        )
      }
  
      if(this.cat=="packs"){
        this.isname_pack=!this.isname_pack;

        this.l.getPack(this.myid).subscribe(
          res=>{this.detail=res;
            for(let elt of this.detail){
              this.price=elt.price;
              localStorage.setItem('price',elt.price)
             }
          },
          err=>{console.log(err)}
        )
      }
  
    }else{
      console.log(this.myid,this.cat);
    }
  
  

  }
}
