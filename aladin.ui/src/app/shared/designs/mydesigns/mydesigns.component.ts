import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-mydesigns',
  templateUrl: './mydesigns.component.html',
  styleUrls: ['./mydesigns.component.scss']
})
export class MydesignsComponent implements OnInit {
 url:any;
  constructor() { }

  ngOnInit(): void {
    this.url=["/assets/images/1.jpg","/assets/images/28.jpg"];
  }

}
