import { Component, OnInit } from '@angular/core';
import { LoginService,FormvalidationService ,AuthinfoService} from 'src/app/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
 data={
   email:"",
   password:"",
   erroremail:false,
   errorpwd:false,
   pwdmsg:"",
   emailmsg:""
 }
 message=""
 show:boolean=false;

  constructor(private loginservice:LoginService,private valideform:FormvalidationService,private route:Router,private authuser:AuthinfoService) { }

  ngOnInit(): void {
  }

  toggleSpinner(){
    this.show = !this.show;

  }

  login(event:Event){
    if(this.valideform.validateEmail(this.data.email)){
      if(this.data.password!=null){
        this.toggleSpinner();
        this.loginservice.login(this.data).subscribe(res=>{
         console.log(res);
         if(res.user!=undefined){
          this.authuser.setItem('access_token',res.user.is_partner);
          this.authuser.setItem('user',res.user.id);
          window.location.href='/users';
         }
         this.toggleSpinner();
         this.data.erroremail=!this.data.erroremail;
         this.message=res.message;   
        
        },err=>{
          this.toggleSpinner();
           if(err.error.text!=undefined){
           this.data.erroremail=!this.data.erroremail;
           this.data.emailmsg=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.data.errorpwd=!this.data.errorpwd;
            this.data.pwdmsg=err.error.password;   
          }
          if(this.data.erroremail!=true&&this.data.errorpwd!=true){
            alert("Une erreur  s'est produite veuillez contactez le service client au 27 23 45 73 02")
          }   
        });
      }
    }else{
      this.data.emailmsg="invalide email";
      this.data.erroremail=!this.data.erroremail;

    }

  }

}
