import { Component, OnInit } from '@angular/core';
import { AuthinfoService } from 'src/app/core';
@Component({
  selector: 'app-header-up',
  templateUrl: './header-up.component.html',
  styleUrls: ['./header-up.component.scss']
})
export class HeaderUPComponent implements OnInit {

  constructor(private auth : AuthinfoService) { }

  ngOnInit(): void {
  }
logout(event:Event)

{
  this.auth.removeItem('access_token');
  window.location.href='/';
 
}


}
