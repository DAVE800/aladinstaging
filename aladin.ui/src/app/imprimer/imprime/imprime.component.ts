import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-imprime',
  templateUrl: './imprime.component.html',
  styleUrls: ['./imprime.component.scss']
})
export class ImprimeComponent implements OnInit {
  printed:any;
  url='/editor/prints/'
  constructor(private l :ListService) { }

  ngOnInit(): void {
    this.l.getPrints().subscribe(
      res=>{
        this.printed=res
      },
      error=>{
        console.log(error);
      }
    )
  }
  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }
}
