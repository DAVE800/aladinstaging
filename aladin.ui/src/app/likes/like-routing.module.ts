import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MylikesComponent } from './mylikes/mylikes.component';
const routes : Routes=[
{path:'',component:MylikesComponent}
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,RouterModule.forChild(routes)
  ]
})
export class LikeRoutingModule { }
