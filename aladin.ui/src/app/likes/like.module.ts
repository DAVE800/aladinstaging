import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LikeRoutingModule } from './like-routing.module';
import { MylikesComponent } from './mylikes/mylikes.component';
import { SharedModule } from '../shared';
@NgModule({
  declarations: [
    MylikesComponent
  ],
  imports: [
    LikeRoutingModule,
    SharedModule,
    
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class LikeModule { }
