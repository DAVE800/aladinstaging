import { SharedModule } from './../shared/shared.module';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AffichageRoutingModule } from './affichage-routing.module';
import { AfficheComponent } from './affiche/affiche.component';


@NgModule({
  declarations: [
    AfficheComponent
  ],
  imports: [
    CommonModule,
    AffichageRoutingModule,
    SharedModule,
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AffichageModule { }
