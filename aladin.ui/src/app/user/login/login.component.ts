import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
password:any
email:any
message={
  email:"",
  password:""
}
epwd=false
maile=false
show=false
  constructor(private loginservice:LoginService) { }

  ngOnInit(): void {
  }

toggle(){
  this.show=!this.show;
}
  login(){
    if(this.password && this.email){
      this.toggle()
      this.loginservice.login({email:this.email,password:this.password}).subscribe(
        res=>{
          if(res.user!=undefined){
            let id=res.user.id;
            alert("Votre compte a été crée vous avez réçu un mail");
            location.href="/checkouts/"+id;
          }
          if(res.message!=undefined){
            this.toggle()
            this.maile=!this.maile;
            this.message.email=res.message; 
          }
          
        },
        err=>{
          this.toggle();
          console.log(err);
          if(err.error.text!=undefined){
           this.maile=!this.maile;
           this.message.email=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.epwd=!this.epwd
            this.message.password=err.error.password;   
          }  
        }
      )
    }
  }
}
