import { Component, Input, OnInit } from '@angular/core';
import { RegisterService } from '../../core';
import { AuthinfoService } from 'src/app/core/storage';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
info="Bienvenue chez ALADIN";
data={
  fname:"",
  lname:"",
  email:"",
  phone:"",
  password:"",
  cpwd:"",
  is_partner:false,
  city:"",
  whatapp:""

}
errmsg={
  password:false,
  email:false,
  phone:false,
  infoemail:"",
  infophone:"",
  infopwd:""
}
show:boolean=false;
ishttpLoaded:boolean=false;
isLoaded:boolean=false;

  constructor(private registerservice: RegisterService,private l:AuthinfoService) { }

  ngOnInit(): void {
    
  }


  toggleSpinner(){
    this.show = !this.show;

  }

  register(event:Event){

    if(this.data.fname!=null&&this.data.lname!=null&&this.data.phone!=null){
      if(this.matchpwd(this.data.password,this.data.cpwd)==false){
        this.errmsg.password=!this.errmsg.password;
        this.errmsg.infopwd="passwords do not macth";
      }else{
        this.toggleSpinner();
        this.registerservice.saveUser({name:this.data.fname + "  "+ this.data.lname,email:this.data.email,phone:this.data.phone,password:this.data.password,is_partner:this.data.is_partner,city:this.data.city,whatsapp:this.data.whatapp}).subscribe(res=>{
         console.log(res);
         if(res.data!=undefined){
          this.l.setItem('access_token',res.role);
          this.l.setItem('user',res.data.insertId);
          window.location.href='/users';
         }

         /**
          * 
         
          */
        },
        err=>{
          this.toggleSpinner();
          console.log(err)
          if(err.error.phone!=undefined){
            this.errmsg.phone=!this.errmsg.phone;
            this.errmsg.infophone=err.error.phone;
       
          }

          if(err.error.password!=undefined){
            this.errmsg.password=!this.errmsg.password;
            this.errmsg.infopwd=err.error.password;
             
          }

          if(err.error.email!=undefined){
            this.errmsg.email=!this.errmsg.email;
            this.errmsg.infoemail=err.error.email;
             
          }


  
        }
        );
      
    }

    }

  }




  matchpwd(pwd:string,cpwd:string):boolean{
    if(pwd===cpwd){
      return true;
    }else{
      return false
    }

  }

}
