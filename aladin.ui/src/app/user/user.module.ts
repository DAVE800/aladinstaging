import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { HomeuserComponent } from './homeuser/homeuser.component';
import { RegisterComponent } from './register/register.component';
import { PwdforgotComponent } from './pwdforgot/pwdforgot.component';
import { InfoComponent } from './info/info.component';
import { SharedModule } from '../shared';
import { LoginComponent } from './login/login.component';
import {OrderHistoriqueComponent }from './order-historique/order-historique.component'

@NgModule({
  declarations: [
    HomeuserComponent,
    RegisterComponent,
    PwdforgotComponent,
    LoginComponent,
    InfoComponent,
    OrderHistoriqueComponent 
  ],
  imports: [
    UserRoutingModule,
    SharedModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class UserModule { }
