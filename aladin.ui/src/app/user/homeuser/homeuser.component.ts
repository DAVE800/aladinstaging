import { Component, OnInit } from '@angular/core';
import { LoginService} from 'src/app/core';
@Component({
  selector: 'app-homeuser',
  templateUrl: './homeuser.component.html',
  styleUrls: ['./homeuser.component.scss']
})
export class HomeuserComponent implements OnInit {
  user:any;
  id:any;
  constructor(private User :LoginService) { }

  ngOnInit(): void {
    this.id = localStorage.getItem('user');
    this.User.getUser(this.id).subscribe(resp=>{
      console.log(resp)
      this.user=resp;
    },
    err=>{
      console.log(err)
    }
    )

    
  }



}
