import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeuserComponent } from './homeuser/homeuser.component';
import { PwdforgotComponent } from './pwdforgot/pwdforgot.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { InfoComponent } from './info/info.component';
import { OrderHistoriqueComponent } from './order-historique/order-historique.component';
import {AuthGuard} from '../core'
const routes: Routes = [
{path:'',
canActivate:[AuthGuard],
 component:HomeuserComponent
},

{path:'register',
component:RegisterComponent},

{path:'pwdforgot',
//canActivate:[AuthGuard],
component:PwdforgotComponent},
{
  path:'login',
  component:LoginComponent
},
{
  path:'info',
  canActivate:[AuthGuard],
  component:InfoComponent
},
{
  path:'orders',
  component:OrderHistoriqueComponent 
}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
