import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderHistoriqueComponent } from './order-historique.component';

describe('OrderHistoriqueComponent', () => {
  let component: OrderHistoriqueComponent;
  let fixture: ComponentFixture<OrderHistoriqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderHistoriqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoriqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
