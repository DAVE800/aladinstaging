import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ClothesRoutingModule } from './clothes-routing.module';
import { SharedModule } from '../shared';
import { VetementComponent } from './vetement/vetement.component';


@NgModule({
  declarations: [VetementComponent ],
  imports: [
    ClothesRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class ClothesModule { }
