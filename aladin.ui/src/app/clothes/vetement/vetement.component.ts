import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-vetement',
  templateUrl: './vetement.component.html',
  styleUrls: ['./vetement.component.scss']
})
export class VetementComponent implements OnInit {
 cltobj:any
 url="/editor/cloth/"
 data="Vêtement : Aladin vous propose des vêtements personnalisés de très bonne qualité avec des finitions qui respectent vos goûts. Vous serez fière de les porter partout quel que soit le lieu et l’événement."
  constructor(private p:ListService) { }

  ngOnInit(): void {
    this.p.getcloths().subscribe(
      res=>{
      this.cltobj=res;
      this.cltobj=this.cltobj.data;
      ;

        },
      err=>{
        console.log(err)

      }

    )
  }



}
