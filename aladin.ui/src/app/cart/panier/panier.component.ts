import { Component, OnInit } from '@angular/core';
import { LocalService } from 'src/app/core';
@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {
 cart_items:any;
 sub_total=0;
 d=0;
 id:any;
 isempty=true;
 diliver=this.d;
  constructor(private item:LocalService) { }

  ngOnInit(): void {

    
   this.cart_items= this.item.cart_items;
   if(this.cart_items.length>0){
    this.isempty=!this.isempty;
    for(let item of this.cart_items){
      this.sub_total=this.sub_total+(+item.t);
   }
   this.diliver= this.diliver+this.sub_total;
   }
   
  }

  removeItem(event:any){
    let id= event.target.id;
    console.log(id)
    if(id!=-1){
      this.item.removeItem(id);
     location.reload()
    }
    
  }

}
