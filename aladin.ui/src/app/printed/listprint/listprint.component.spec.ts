import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListprintComponent } from './listprint.component';

describe('ListprintComponent', () => {
  let component: ListprintComponent;
  let fixture: ComponentFixture<ListprintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListprintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
