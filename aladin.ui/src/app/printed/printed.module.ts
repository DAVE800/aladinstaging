import { SharedModule } from '../shared';
import { CommonModule } from '@angular/common';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PrintedRoutingModule } from './printed-routing.module';
import { ListprintComponent } from './listprint/listprint.component';


@NgModule({
  declarations: [
    ListprintComponent
  ],
  imports: [
    CommonModule,
    PrintedRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintedModule { }
